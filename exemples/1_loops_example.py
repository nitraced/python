'''
Created on 1 May 2021

@author: jcano
'''


### Exemple de boucle for

notes_classe = [12.0,16.3,5.5,10.0,15.0,14.0,2.5,14.0]
total=0
for note in notes_classe:
    total+=note 
moyenne=total/len(notes_classe)
print(moyenne)

### Exemple de boucle for et de dénombrement 

list = []
for i in range(1,200): # pour tous les entiers
    if i%7==0: #si i modulo 7 est nul
        list.append(i) # Ajout a la liste
print(list) # On s'amuse a les aficher
print(len(list))

### Exemple de boucle while
valeur = 3.14159
erreur = 9999 # valeur aberrante
numerateur=1
denominateur=1 # On force les operations en point flottant 
iter = 0
while abs(erreur)>0.01: # Tant que la valeur absolue n'est pas satisfaisante
    if erreur<-0.01: 
        numerateur+=1 # On est trop bas!!!
    else:
        denominateur+=1 # On est trop haut!!!
    erreur = float(numerateur)/float(denominateur) - valeur # Calcul de l'erreur
    # Attention aux types!!!!
print("Pi = " + str(numerateur) + "/" + str(denominateur))
