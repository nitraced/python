'''
Created on 1 May 2021

@author: jcano
'''

## Exemple de programmation procédurale 
eleves = ["Jose","Axel","Agathe"]
ages = [62,26,3]
# Declaration des fonctions
def print_eleve(nom,age):
    string = nom + " qui a " + str(age) + " ans"
    print(string)

def moyenne_age(tab_ages):
    s=0
    for age in tab_ages:
       s+=age     
    s/=len(tab_ages) 
    return s # On retourne la moyenne 

# Execution du code 
print("Dans la classe, il y a :")
for i in range(len(eleves)):
    print_eleve(eleves[i],ages[i])
print("Moyenne d'age du groupe " + str(int(moyenne_age(ages))) + " ans")


## Exemple de recursion

def compte(somme,taux):
    somme = somme*taux + 16.0 # Recursion sur l'argent 
    taux = taux*1.03 #Recursion sur le taux
    return somme, taux 

somme = 10000.0 
taux = 1.02 
annee = 0
while somme<20000:
    somme,taux = compte(somme,taux)
    # Recursion
    annee+=1
print("Rentable a partir de "+str(annee)+" ans")


 