import csv
import os
from datetime import date

with open("sentence.txt","r") as f:
	lecteur = csv.reader(f,delimiter=" ")
	ligne = next(lecteur) # La ligne est seule dans le fichier
	ligne.reverse() # On inverse la liste
	f.close() # On ferme le fichier

if not os.path.exists("verlan"): # Test d'existence
	os.mkdir("verlan") #creation du repertoire
os.chdir("verlan") #on va dans le repertoire
nom_fichier = str(date.today()) #nom du fichier

f = open(nom_fichier+".txt","w") #Creation du nouveau fichier 
sw = "" # String vide
for s in ligne:
	sw+=" "+s # Compression du tableau dans un seul string
f.write(sw) # On ecrit le texte en entier dans le fichier
f.close() # On ferme le fichier 
