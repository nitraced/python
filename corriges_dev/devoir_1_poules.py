#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#  poules.py
#  
#  Copyright 2021 Justin Cano <cano.justin@gmail.com>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  


def main(args):
	print("Comptons les poules")
	age_des_poules = [10,0,0,0,0,0]
	for annee in range(20): # Pour chaque année
		age_en_cours = 0 # On commence à compter pour les poussins
		poules_adultes = 0 # On n'a pas encore commencé notre rescencement 
		for population_age in age_des_poules: # Pour tous les ages des poules
			if age_en_cours>1: # Maturité de la poule
				poules_adultes+=population_age # On compte les poules matures
			age_en_cours = age_en_cours+1 # On explore l'age suivant
		# On a le compte des poules adultes, il faut faire vieillir la population
		for i in [4,3,2,1,0]:
			age_des_poules[i+1]=age_des_poules[i] # On fait vieillir la population
		age_des_poules[0]=int(3*poules_adultes/2) # Trois poussins par couple (partie entière)
		print("Année en cours "+str(annee))
		print(age_des_poules)
	#Sommer les poules quel que soit leur age
	total = 0
	for poules_d_un_certain_age in age_des_poules:
		total+=poules_d_un_certain_age
	print("\n Total de la population :" + str(total))
	return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
