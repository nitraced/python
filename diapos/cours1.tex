\documentclass[11pt]{beamer}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[french]{babel}
\usetheme{EastLansing}
\usepackage{amsthm}

\usepackage{color}
\definecolor{deepblue}{rgb}{0,0,0.5}
\definecolor{deepred}{rgb}{0.6,0,0}
\definecolor{deepgreen}{rgb}{0,0.5,0}

\usepackage{pythonhighlight}


\newtheorem{dfn}{Définition}
\newtheorem{qst}{Question}
\newenvironment{ex}{\begin{exampleblock}{Exemple}}{\end{exampleblock}}

%\newenvironment{pyframe}{\begin{frame}[fragile,allowframebreaks]}{\end{frame}}




\AtBeginSection[]
{
	\begin{frame}<beamer>
	\frametitle{Plan de la section \thesection}
	\tableofcontents[currentsection]
\end{frame}
}

\begin{document}


	
	\author{Justin Cano}
	\title{Introduction à Python}
	\subtitle{Cours 1 : notions fondamentales}
	%\logo{}
	\institute[DGE Poly]{Polytechnique Montréal \\ Département de Génie Électrique.}
	\date{5 mai 2021}
	%\subject{}
	%\setbeamercovered{transparent}
	%\setbeamertemplate{navigation symbols}{}
	\begin{frame}[plain]
	\maketitle
\end{frame}


\begin{frame}
\tableofcontents
\end{frame}

\section{Contexte}

\begin{frame}{Qu'est-ce que Python?}

\begin{block}{Quelques propriétés du serpent}
	\begin{itemize}
	\item Python est un langage informatique interprété, ce qui lui permet d'être indépendant de la plateforme qui l'exécute. 
	
	\item C'est un langage de type script, utilisant la hiérarchisation par indentation. 
	
	\item Il s'agit aussi d'un langage non-typé (c'est-à-dire sans déclaration \textit{a priori}, chose qui peut être un \textit{Pharmakôn}\footnote{Antidote et poison, notion platonicienne.} : salvatrice car rapide mais trompeuse car simpliste).
\end{itemize}
\end{block}




\begin{block}{Depuis quand existe-il?}
	Il fut mis au point en 1991, par l'ingénieur néerlandais \emph{Guido van Rossum} (né en 1956) et nommé ainsi en l'hommage aux Monty Pythons, dont Rossum était un inconditionnel.
\end{block}
\end{frame}


\section{Les variables}

\begin{frame}{\textit{Nota bene}}
Dans cette section, nous exécuterons des opérations élémentaires directement dans \textbf{la console Python}. Ce qui est rapide (on n'a pas à sauvegarder un fichier et on peut voir étape par étape ce qu'il se passe). 

\begin{description}
	\item[Sur Linux] lancer la commande \texttt{python3} dans un terminal : la console apparaîtra.
	\item[Sur Windows] exécutez ceci dans \textit{IDLE}, la console qui vient avec le téléchargement de \texttt{Python}.
\end{description}
\end{frame}

\subsection{Mémoire et allocation}

\begin{frame}[fragile,allowframebreaks]{Mémoire et allocation}

D'un point de vue sémantique coder c'est \textbf{jouer avec des variables} : l'étymologie du terme \textit{algorithmique}\footnote{Signifiant calcul en arabe.} en atteste. 

Cependant, en pratique, il s'agit de placer des données quantifiées en bits dans la mémoire vive. Python gère cela de manière automatisée, à la manière de \texttt{Java} mais à l'inverse de \texttt{C/C++} (voir la notion de \textit{pointeur}).

\begin{dfn}
	On appelle \textbf{adresse} l'endroit où se trouve stockée la \textbf{valeur} d'une \textbf{variable}. 
\end{dfn}

\begin{ex}
On affecte dans python la variable $n$ à la valeur $2$ :
\begin{python}
>>> n = 2
\end{python}
où se trouve cette dernière ? 
\end{ex}
\framebreak 
Pour répondre à la question précédente on va utiliser la routine \pyth{id()} :
\begin{python}
>>> id(n)
94539112154496	
\end{python}

dans \textbf{mon ordinateur} \footnote{Et pas nécessairement le votre, le mien a pris cette décision aujourd'hui car il y'avait de la place dans ces octets-ci.}, cette variable voit son \textbf{premier octet} occuper la $94539112154496$-ème case de la mémoire vive.

\textbf{Rassurez-vous} : \texttt{Python} gère ceci tout seul et sait retrouver les variables.

\end{frame}

\subsection{Copie profonde et copie superficielle}

\begin{frame}[fragile,allowframebreaks]{Copie profonde et copie superficielle}
Disons qu'on veuille maintenant jouer avec une autre variable $m$ qu'on va, par paresse, affecter à la même valeur que $n$ :

\begin{python}
>>> m = n 
\end{python}

\begin{qst}
	Où est-située $m$?
\end{qst}

\begin{python}
>>> id(m)
94539112154496
\end{python}

Ce nombre me dit quelque chose... je vais \textbf{comparer les deux adresse} pour en être sûr (\pyth{==} est un opérateur de test d'égalité)...
\begin{python}
>>> id(m)==id(n)
True
\end{python}
Ce sont les mêmes adresses! 

Toutefois... on peut remarquer 
\begin{python}
>>> n=2
>>> m=n
>>> id(m)==id(n)
True
>>> m=4222
>>> id(m)==id(n)
False
>>> m
4222
\end{python}


\textbf{Conclusion?} Python va déplacer $m$ car il «voit» qu'on a choisi une autre valeur. Mais il peut ne pas le faire... 

\framebreak 
En particulier, lorsqu'on manipule des objets (entités composées de plusieurs variables, dont nous reparlerons), on peut avoir quelques surprises.

\begin{ex}
Supposons un tableau contenant deux nombres :
\begin{python}
>>> tableau = [1958,1994]
\end{python}
\end{ex}
Je veux faire une copie de ce dernier :
\begin{python}
>>> nouveau_tableau = tableau 
\end{python}
Je change d'avis... je veux modifier le premier élément du nouveau tableau :
\begin{python}
>>> nouveau_tableau[0]=2021
\end{python}
\framebreak 
Et là c'est le drame... 
\begin{python} 
>>> nouveau_tableau
	[2021, 1994]
>>> tableau
	[2021, 1994]
\end{python}

Les deux tableaux sont identiques! Modifier l'un entraîne la modification de l'autre... 

\begin{alertblock}{Solution}
	Procéder à la copie profonde des objets.
\end{alertblock}

\framebreak 
\begin{ex}
	Retour à l'exemple :
\end{ex}

On importe le module \textbf{copie} :
\begin{python}
	>>> import copy
	>>> tableau = [1958,1994]
\end{python} 
on fait deux copies d'objets :
\begin{python}
>>> copie_superficielle = copy.copy(tableau)
>>> copie_profonde = copy.deepcopy(tableau)
\end{python}

\framebreak 
On remarque que les deux copies ont donné un bon résultat ce coup-ci (seul «tableau» est modifié) :
\begin{python}
>>> tableau[0]=2021
>>> tableau
[2021, 1994]
>>> copie_superficielle
[1958, 1994]
>>> copie_profonde
[1958, 1994]
\end{python}
\textbf{La différence} entre les deux est que si on avait des \textbf{objets imbriqués} (tableaux de tableaux par exemple) on n'aurait pas eu de copie des objets en question.

\framebreak 
\begin{ex}
	Soit le tableau contenant un tableau contenant l'élément de valeur $500$. On procède aux deux copies :
	\begin{python}
	>>> tableau = [500]
	>>> tableau_du_tableau = [tableau]
	>>> copie_superficielle = copy.copy(tableau_du_tableau)
	>>> copie_profonde = copy.deepcopy(tableau_du_tableau)
	\end{python}
\end{ex}
Modifions l'unique case (case zéro du zéroième tableau) en lui donnant une valeur qui est un texte «\textit{hello}» :
\begin{python}
tableau_du_tableau[0][0]="hello"
\end{python}

Résultat de la manœuvre :
\begin{python}
>>> copie_superficielle
[['hello']]
>>> copie_profonde
[[500]]
\end{python}
\textbf{CQFD :} on voit tout de suite la différence... la copie superficielle voit son élément (le tableau) pointer vers la même adresse...

\end{frame}

\subsection{Les types}

\begin{frame}[fragile,allowframebreaks]{Les types}

\begin{dfn}
	Il s'agit de structures de données. C'est-à-dire une matérialisation standardisée des \textbf{variables} dans la \textbf{mémoire} composée d'une série de $0/1$ (bits).
\end{dfn}

\framebreak 
\textbf{Liste :}   (non exhaustive, types principaux) 

\begin{description}
	\item[Booléens] types ne pouvant que prendre les valeurs \pyth{True} ou \pyth{False}. Nominativement, ils ne prennent qu'un bit de mémoire, mais cela est erroné : les «cases mémoire» sont en réalité souvent regroupées par paquet de huit bits, qu'on appelle \textbf{octets}. L'opérateur de conversion est la fonction \pyth{bool()} qui permet de transformer toute variable en booléen.
	\item[Entiers] En \texttt{Python 3} il n'y a qu'un type d'entiers (signés) qui va sélectionner automatiquement le nombre de case mémoire dont il a besoin (les entiers codés sur $N$ bits peuvent appartenir à l'intervalle $[-2^{N-1}+1,2^{N-1}]$). L'opérateur de conversion est la fonction \pyth{int()}.
	\item[Flotants] les nombres \textbf{flotants} sont des nombres à virgule, généralement encodés sur huit octets \footnote{Appelés \texttt{double} en \texttt{C} car ils comportent 64 bits ($64 = 8 \times 8$), soit le \textbf{double} des traditionnelles architectures 32 bits, populaires lorsqu'ils furent instaurées. En effet, en \texttt{C}, le \texttt{float} de base est de $32$-bits, convention reprise par d'autres langages (\texttt{C++},\texttt{Java}...).}. Le fait est qu'ils diffèrent des entiers de par leur structure à point flottant, contrairement aux premiers à points fixes.
	
	Sans entrer dans les détails, disons que les flottants sont comme de l'écriture scientifique.
	\begin{ex}
    Soit deux nombres entiers $s = 314$ (chiffres significatifs) et $e = -2$ (exposant), soit un nombre à virgule $v = \pi$. On peut (avec une certaine précision dépendant de $s$) l'encoder dans le flottant $f = [s,e]$ qui sera lu ainsi par la machine :
	\[
		v \approx s \times 10^e \Leftrightarrow \pi \approx 314 \times 10^{-2} = 3,14.
	\]
\end{ex}
	L'opérateur de conversion des floats est la fonction \pyth{float()}. 
	
	\textbf{Astuce :} mettre un $.0$ à la fin d'une déclaration de nombre entier va créer un flottant. 
	\begin{ex} 
	\pyth{f = 25.0} est reconnu comme un flottant tandis que \pyth{i=25} est reconnu comme un nombre entier.
	\end{ex} 

	\item[String] Appelées chaînes de caractères qui encodent les caractères alphanumériques. Contrairement aux langages de plus bas niveau que \texttt{Python}, tels le \texttt{C}, les notions de caractère (élément) et de chaîne (tableaux de caractère) sont liées. Ainsi \pyth{a="y"} et \pyth{b="yaourt"} sont des \texttt{string}.
	L'opérateur de conversion des string (\textbf{très utile pour afficher du texte sur la console}) est la fonction \pyth{str()}.
\end{description}

\end{frame}


\subsection{Opérateurs binaires}

\begin{frame}[fragile]{Opérateurs mathématiques de base}
Soient \pyth{x} et \pyth{y} deux variables, voici les syntaxes de base des opérations élémentaires :
\begin{description}
	\item[Addition :] \pyth{x+y};
	\item[Soustraction :] \pyth{x-y};
	\item[Multiplication :] \pyth{x*y};
	\item[Division :] \pyth{x/y};
	\item[Division entière :] \pyth{x//y} (condensé de \pyth{int(x/y)});
	\item[Modulo :] \pyth{x%y}, c'est-à-dire $x~[y]$;
	\item[Puissance :] \pyth{x**y}, c'est-à-dire $x^y$.
\end{description}

\textbf{Note : } L'addition marche aussi sur les «string», cela les concatène : \pyth{"J'aime "+"les yaourts"} donnera \pyth{"J'aime les yaourts"}.

\end{frame}


\begin{frame}[fragile]{Opérateurs mathématiques : notation condensée}
Soient \pyth{x} et \pyth{y} deux variables, voici les syntaxes des opérations condensées ($x$ est impliqué deux fois dans l'équation)
\begin{description}
	\item[Incrémentation :] \pyth{x+=y} $\Leftrightarrow x=x+y$;
	\item[Soustraction :] \pyth{x-=y} $\Leftrightarrow x=x-y$;
	\item[Gain \footnote{Ou facteur d'échelle.} :] \pyth{x*=y} $\Leftrightarrow x=x*y$;
	\item[Normalisation :] \pyth{x/=y} $\Leftrightarrow x=x/y$;
\end{description}


\end{frame}

\begin{frame}[fragile]{Opérateurs de comparaison}
Soient \pyth{x} et \pyth{y} deux variables numériques (flottantes ou entières), les opérateurs de comparaison produisent un \textbf{Booléen} qui est le résultat du test :
\begin{description}
	\item[Égalité :] \pyth{x==y};
	\item[Différence :] \pyth{x!=y};
	\item[Inégalité :] \pyth{x<y} (stricte) \pyth{x=<y} (large);
	\item[Inégalité(bis) :] \pyth{x>y} (stricte) \pyth{x=>y} (large); \\
\end{description}



\end{frame}

\begin{frame}[fragile]{Opérateurs logiques}
Soient \pyth{x} et \pyth{y} deux variables booléennes (qui peuvent être des résultats de test)

\begin{description}
	\item[Égalité :] \pyth{x==y};
	\item[Différence :] \pyth{x!=y};
	\item[et :] \pyth{x&&y} ou bien \pyth{x and y} (résultat de «si $x$ et $y$ sont vrais»);
	\item[ou :] \pyth{x||y} ou bien \pyth{x or y} (résultat de «si $x$ ou $y$ sont vrais»);
	\item[ou exclusif:] \pyth{x^y} (résultat de «si \textbf{soit} $x$ \textbf{soit} $y$ est vrai, mais \textbf{pas les deux à la fois}»).
\end{description}

\end{frame}


\subsection{Impression de variables}

\begin{frame}[fragile,allowframebreaks]{Impression de variables}
Car la console c'est bien mais... si on veut un jour écrire des fichiers de code plus complexe (et les sauvegarder) on doit s'en passer.

\begin{qst}
	Comment afficher alors les résultats des programmes?
\end{qst}

On peut demander une impression des résultats dudit script dans la console, et ceci en utilisant la routine \pyth{print()} qui prend en argument un \textbf{string}.

\framebreak 

\begin{ex}
	On veut afficher un nombre $n$ avec un peu de texte autour.
\end{ex}

Le code proposé est :
\begin{python}
n = 2
string = "Le nombre que je veux afficher est " + str(n)
string+=" !"
print(string)
\end{python}
on l'enregistre dans un fichier \texttt{1\_print\_example.py} (l'extension \texttt{.py} est celle du python) et on l'exécute, on obtient le résultat suivant dans la console:
\begin{verbatim}
Le nombre que je veux afficher est 2 !
\end{verbatim}
Remarquer la conversion (cast) d'entier (\texttt{int}) vers \texttt{string} avec \pyth{str()} et l'incrémentation \pyth{+=} qui rajoute le point d'exclamation dans la chaîne de caractère. En effet, le code suivant donne une erreur :
\begin{python}
>>	string = "Le nombre que je veux afficher est " + n 
TypeError: must be str, not int
\end{python}
une erreur qui est de type. On ne peut ajouter deux types différents.

Toutefois... \pyth{print(n)} fonctionne et retourne \texttt{2}. Les développeurs de \texttt{Python} ont rendu flexible ce dernier, rigoureusement c'est \pyth{print(str(n))} qu'il faudrait écrire. 
\end{frame}


\section{Opérations automatisées}
\subsection{Listes}

\begin{frame}[fragile,allowframebreaks]{Listes}
\begin{dfn}
	Une liste est un ensemble ordonné d'éléments de même nature. 
\end{dfn}
On peut voir ces dernières comme un tableau. Rigoureusement, il s'agit d'une \textbf{classe} au sens \textbf{programmation objet du terme}. C'est pour cela que les listes ont quelques fonctions (méthodes) qui leur sont propres.

\begin{ex}
	Définissons la liste de chaîne de caractères suivante :
\begin{python}
personnes = ["Jose","Axel","Justin"]
\end{python} 
\end{ex}

\framebreak 

Les listes sont caractérisées par leurs éléments, on appelle leur \textbf{nombre} la \textbf{longueur de cette dernière} qu'on peut extraire grâce à \pyth{len()}
\begin{ex}
\begin{python}
print(len(personnes))
\end{python}
la précédente commande donnera $3$ sur la console.
\end{ex}

Pour accéder au $i$-eme élément d'une liste \pyth{l} on doit écrire \pyth{l[i]}.
Ainsi, dans notre exemple \pyth{personnes[1]}, le deuxième élément, contient la valeur \pyth{"Axel"}.
\\
\textbf{Note : } Remarquer que les listes de longueur $l$ sont indexées de $0$ à $l-1$. \\
\textbf{Attention :} dans notre exemple,  \pyth{personnes[3]} donne une erreur : il n'\textbf{existe pas} de quatrième élément.
\\
\textbf{Astuce :} le dernier élément d'une liste peut être indexé par $-1$ :  \pyth{personnes[-1]=personnes[2]="Justin"}. Généralisable aux avant-derniers $-2$ et etc...

\begin{itemize}
\item On affecte une valeur à un élément de liste ainsi : \pyth{liste[index]=valeur}
\item On lit une case d'une liste ainsi : \pyth{valeur=liste[index]}
\end{itemize}

\framebreak 

\begin{dfn}[Dépilage et empilage] 
	On peut retirer le dernier élément d'une liste (dépilage, \pyth{pop()}) ou au contraire en ajouter un (empilage,  \pyth{append()}) 
\end{dfn}

\begin{ex}
\begin{python}
>>> personnes = ["Jose","Axel","Justin"]
>>> personnes.pop()
'Justin'
>>> personnes
['Jose', 'Axel']
>>> personnes.append("Agathe")
>>> personnes
['Jose', 'Axel', 'Agathe']
\end{python}
\end{ex} 
\small 
\textbf{Note :} il existe également les routines \pyth{liste.remove(index)} pour supprimer des éléments et  \pyth{liste.insert(index)} pour en insérer.


\begin{dfn}[Concaténation]
	Déjà vue pour les \pyth{string}, elle est généralisable à toutes les listes \textbf{pourvu qu'elles contiennent le même type de données}.
\end{dfn}

\begin{ex}
	\begin{python}
>>> notes_axel = [20.0,15.0,13.0]
>>> notes_jose = [12.0,22.0,3.1459]
>>> notes_classe = notes_axel + notes_jose
>>> notes_classe
[20.0, 15.0, 13.0, 12.0, 22.0, 3.1459]
	\end{python}
\end{ex}
 \textbf{Astuce :} si par hasard vous voulez initialiser une liste vite, faites \pyth{liste=[]}. Cela peut être utile.


\begin{dfn}[Tri]
	On peut également trier une liste \textbf{numérique} par valeurs croissantes ou décroissante facilement avec \pyth{sort()} et \pyth{reverse()}. 
\end{dfn}

\begin{ex}
\begin{python}
>>> notes_classe.sort()
[3.1459, 12.0, 13.0, 15.0, 20.0, 22.0]
>>> notes_classe.reverse()
>>> notes_classe
[22.0, 20.0, 15.0, 13.0, 12.0, 3.1459]
\end{python}
\end{ex}
\textbf{Remarque :} les fonctions présentées ci-haut sont des fonctions qui sont \textbf{propres} à chacun des objets considérés. On écrira \pyth{liste.sort()} et non pas \pyth{liste(sort)} .

\end{frame}


\subsection{Boucles déterminées}

\begin{frame}[fragile]{Itérer sur les éléments d'une liste}
Dans le cas où l'on souhaite faire ceci, on peut invoquer une boucle \pyth{for}. C'est une boucle à laquelle on fournit une liste sur laquelle elle ira itérer. En voici la syntaxe :
\begin{python}
for i in liste:
	instructions
\end{python}

\begin{ex} Calcul de moyenne de classe :
\begin{python}
notes_classe = [12.0,16.3,5.5,10.0,15.0,14.0,2.5,14.0]
total=0
for note in notes_classe:
	total+=note 
moyenne=total/len(notes_classe)
print(moyenne)		
\end{python}
la réponse est $11.16$... pas fameux.
\end{ex}

\end{frame}

\begin{frame}[fragile]{Hiérarchie en Python}
\textbf{Remarque importante :}
L'indentation est \textbf{la clef} en \texttt{Python}. Une boucle (qui est un test «tant que»), un autre test ou une déclaration de fonction (voir plus tard) voit nécessairement du code lui être subordonnée.

L'instruction qui lance ce code est marquée d'un «:» tandis que le code qu'elle subordonne est indenté (tabulation). 

Ainsi, on a la syntaxe suivante :
\begin{python}
test ou declaration de procedure:
	lignes de code subordonnees (niveau 1)
	test:
		lignes de codes subordonnees (niveau 2)
	lignes de code subordonnees (niveau 1)
\end{python}
Ainsi \textbf{une mauvaise indentation} peut faire \textbf{échouer un programme}.
\end{frame}

\begin{frame}[fragile]{Que faire si on veut itérer juste $N$ fois (sans liste) ?}

En créant... une liste. La fonction \pyth{range(N)} crée une liste $l$ de $N$ éléments entiers commençant par $0$ :
\[
l = [0,1 \dots N-1] \in \mathbb{N}^N
\]
on peut également spécifier d'autres options à range : ne pas commencer la liste en zéro $0$, sortir $N$ nombres pairs... etc...

\begin{ex} Dénombrer et afficher les nombres entiers multiples de $7$ entre $1$ et $200$
\begin{python}
list = []
for i in range(1,200): # pour tous les entiers
	if i%7==0: #si i modulo 7 est nul
		list.append(i) # Ajout a la liste
print(list) # On s'amuse a les aficher
print(len(list)) # On les compte
\end{python}
	on trouve 28 nombres dans ce cas.
\end{ex}

\end{frame}


\subsection{Boucles à tests}

\begin{frame}[fragile,allowframebreaks]{Boucles conditionnelles}

Cette classe de boucle va faire tourner un code \textbf{tant que} (\textit{while}) la condition spécifiée est vraie. Voici la syntaxe d'une telle boucle :
\begin{python}
while condition:
	instructions
\end{python}

\framebreak 
\begin{ex}
Approximer par une fraction entière à 0.01 près:

\begin{python}
valeur = 3.14159
erreur = 9999 # valeur aberrante
numerateur=1
denominateur=1
while abs(erreur)>0.01: #Tant que la valeur absolue de l'erreur est mauvaise
	if erreur<-0.01: 
		numerateur+=1 # On est trop bas!!!
	else:
		denominateur+=1 # On est trop haut!!!
	erreur = float(numerateur)/float(denominateur) - valeur 
# Attention aux types la division doit etre un flottant!

print("Pi = " + str(numerateur) + "/" + str(denominateur))
\end{python}
Réponse... les fameux $22/7$ de l'école primaire.
\end{ex}

\textbf{Attention :} si la condition est toujours vraie... cela fait une boucle infinie.
\end{frame}


\section{Procédures}
\subsection{Fonctions}

\begin{frame}[fragile]{Les fonctions}
Parfois, lorsqu'on construit du code... on se répète. Ou on se répéte à quelques paramètres près. 
Voyons comment faire pour se simplifier la tâche.

\begin{dfn}[Fonction]
	Une fonction est une procédure appelable avec des paramètres en argument (entrée) ainsi que possiblement des résultats en sortie.
\end{dfn}
La syntaxe de \textbf{déclaration} est la suivante :
\begin{python}
def ma_fonction(param1,param2):
	instructions
	resultat1 = ....
	resultat2 =
	return resultat1,resultat2 	
\end{python}
lorsqu'on voudra l'invoquer, on écrira \pyth{resultat_1,resultat2 = ma_fonction(param1,param2)}.
\end{frame}

\begin{frame}[fragile,allowframebreaks]{Exemple d'usage}
\begin{ex}
	
\end{ex}
On veut coder rapidement un programme qui comporte les fonctionnalités suivantes :
\begin{enumerate}
	\item Afficher les prénoms d'une liste d'élève d'une classe;
	\item Précisant leur âge respectif;
	\item Précisant la moyenne d'âge de la classe.
\end{enumerate}

\framebreak 
\begin{python}
eleves = ["Jose","Axel","Agathe"]
ages = [62,26,3]
# Declaration des fonctions
def print_eleve(nom,age):
	string = nom + " qui a " + str(age) + " ans"
	print(string)
def moyenne_age(tab_ages):
	s=0
	for age in tab_ages:
		s+=age # somme des ages    
	s/=len(tab_ages) # moyenne (flottant)
	return str(int(s)) # partie entiere et string 
# Execution du code 
print("Dans la classe, il y a :")
for i in range(len(eleves)):
	print_eleve(eleves[i],ages[i])
print("Moyenne d'age du groupe "+moyenne_age(ages)+" ans")
\end{python}

Qui donne le résultat suivant :
\begin{verbatim}
Dans la classe, il y a :
Jose qui a 62 ans
Axel qui a 26 ans
Agathe qui a 3 ans
Moyenne d'age du groupe 30 ans
\end{verbatim}
et ce avec un code assez compact.

\end{frame}

\subsection{Opérations récursives}

\begin{frame}[fragile,allowframebreaks]{Exemple d'opérations récursives}
\begin{ex}
	 Supposons qu'on veuille savoir la date à laquelle une épargne de $10000\$$ aura doublé. Son taux initial est de $2 \%$, pour chaque année passée dans la banque, ce taux augmente lui-même de $3 \%$. Coder une routine faisant le calcul annuel de l'argent dans le compte, sachant que vous y versez $16\$$ annuels.
\end{ex}

\framebreak 
\begin{python}
#Routine de recursion
def compte(somme,taux):
	somme = somme*taux + 16.0 # Recursion sur l'argent 
	taux = taux*1.03 #Recursion sur le taux
	return somme, taux 

somme = 10000 
taux = 1.02 
annee = 0
while somme<20000: #Tant que cela n'a pas double
	somme,taux = compte(somme,taux)
	annee+=1
print("Rentable a partir de "+str(annee)+" ans")
\end{python} 	

La réponse est 7 ans!

\end{frame}



\begin{frame}{The end}
\centering
 Merci pour votre attention.
 \vfill  
 \textbf{Exercices suggérés} :
 \begin{itemize}
 	\item Refaire les exemples;
 	\item S'aider du code-compagnon (voir archive en \texttt{.zip});
 	\item Inventez-vous quelques \textit{scenarii} pratiques à partir de ces derniers.
 \end{itemize}
 
\end{frame}

\end{document}