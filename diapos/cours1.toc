\babel@toc {french}{}
\beamer@sectionintoc {1}{Contexte}{3}{0}{1}
\beamer@sectionintoc {2}{Les variables}{5}{0}{2}
\beamer@subsectionintoc {2}{1}{M\IeC {\'e}moire et allocation}{7}{0}{2}
\beamer@subsectionintoc {2}{2}{Copie profonde et copie superficielle}{9}{0}{2}
\beamer@subsectionintoc {2}{3}{Les types}{17}{0}{2}
\beamer@subsectionintoc {2}{4}{Op\IeC {\'e}rateurs binaires}{21}{0}{2}
\beamer@subsectionintoc {2}{5}{Impression de variables}{25}{0}{2}
\beamer@sectionintoc {3}{Op\IeC {\'e}rations automatis\IeC {\'e}es}{28}{0}{3}
\beamer@subsectionintoc {3}{1}{Listes}{29}{0}{3}
\beamer@subsectionintoc {3}{2}{Boucles d\IeC {\'e}termin\IeC {\'e}es}{35}{0}{3}
\beamer@subsectionintoc {3}{3}{Boucles \IeC {\`a} tests}{38}{0}{3}
\beamer@sectionintoc {4}{Proc\IeC {\'e}dures}{40}{0}{4}
\beamer@subsectionintoc {4}{1}{Fonctions}{41}{0}{4}
\beamer@subsectionintoc {4}{2}{Op\IeC {\'e}rations r\IeC {\'e}cursives}{45}{0}{4}
